import math

print("Расчет коэффициента загрузки\n")
l = int(input("Введите интенсивность входного потока λ: "))
m = int(input("Введите производительность обработки потока запросов μ: "))
n = int(input("Введите емкость буфера n: "))
k = int(input("Введите каналы обработки запросов k: "))
p = round(l / m, 3)
print("Коэффициент загрузки P: ", p, "\n")
ps = round(p / k, 3)
print("Коэффициент загрузки СМО Ps: ", ps, "\n")

result = 0
for j in range(k):
    result += (pow(p,j))/math.factorial(j)
po = round(1 / (result + (pow(p,k) * (1 - pow(ps,n+1))) / (math.factorial(k)*(1-ps))), 8)
print("Показатель простоя: ", po, "\n")

pden = round((pow(p, k+n) * po) / (math.factorial(k)* pow(k,n)), 3)
print("Вероятность отказа в дальнейшем обслуживании системы: ", pden, "\n")

kch = round(p * (1-pden), 3)
print("Среднее количество каналов, которые остались занятыми и запросов в очереди: ", kch, "\n")

wreq = round(((pow(p, k+1) * po)/(math.factorial(k) * k)) * (1 - pow(ps,n)*(n+1-n*ps))/pow(1-ps, 2), 3)
print("Cредний показатель запросов, стоящих в очереди: ", wreq, "\n")

treq = round(wreq / l, 3)
print("Cреднее время, потраченное системой на ожидание перед обработкой в очереди: ", treq, "\n")

ws = round(kch + wreq, 3)
print("Среднее количество запросов в СМО: ", ws, "\n")

ts = round(treq + (1-pden)/m, 3)
print("Cреднее время, в течение которого запросы находятся в СМО: ", ts, "\n")